/* speedie's tabbed fork
 * https://codeberg.org/speedie/tabbed
 *
 * You can set most of these options in your .Xresources
 * Check the 'man' page if you're unsure how to use tabbed.
 */

static char font[]              = "NotoSans-Regular:size=8";
static char* normbgcolor        = "#222222";
static char* normfgcolor        = "#cccccc";
static char* selbgcolor         = "#222222";
static char* selfgcolor         = "#ffffff";
static char* urgbgcolor         = "#111111";
static char* urgfgcolor         = "#cc0000";
static char shell[]             = "/bin/sh";
static const char before[]      = "<";
static const char after[]       = ">";
static const char titletrim[]   = "...";
static int tabwidth             = 200;
static int barHeight            = 18;
static const Bool foreground    = True;
static       Bool urgentswitch  = False;

static int tagwidth				= 3;
static char* tagcolor			= "#af2626";

/*
 * Where to place a new tab when it is opened. When npisrelative is True,
 * then the current position is changed + newposition. If npisrelative
 * is False, then newposition is an absolute position.
 */
static int  newposition   = 0;
static Bool npisrelative  = False;

#define SETPROP(p) { \
        .v = (char *[]){ shell, "-c", \
                "prop=\"`xwininfo -children -id $1 | grep '^     0x' |" \
                "sed -e's@^ *\\(0x[0-9a-f]*\\) \"\\([^\"]*\\)\".*@\\1 \\2@' |" \
                "xargs -0 printf %b | dmenu -l 10 -w $1`\" &&" \
                "xprop -id $1 -f $0 8s -set $0 \"$prop\"", \
                p, winid, NULL \
        } \
}

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "font",         STRING,  &font },
		{ "color0",       STRING,  &normbgcolor },
		{ "color4",       STRING,  &normfgcolor },
		{ "color5",       STRING,  &selbgcolor },
		{ "color7",       STRING,  &selfgcolor },
		{ "color2",       STRING,  &urgbgcolor },
		{ "color3",       STRING,  &tagcolor },
		{ "color4",       STRING,  &urgfgcolor },
        { "shell",        STRING,  &shell },
		{ "tagwidth",     STRING,  &tagwidth },
        { "tabwidth",     INTEGER, &tabwidth },
		{ "barheight",    INTEGER, &barHeight },
};

static Key keys[] = {
	/* modifier             key           function     argument */
	{ MODIFIER1|SHIFT,      XK_Return,    focusonce,   { 0 } },
	{ MODIFIER1,            XK_Return,    spawn,       { 0 } },
	{ MODIFIER1|SHIFT,      XK_l,         rotate,      { .i = +1 } },
	{ MODIFIER1|SHIFT,      XK_h,         rotate,      { .i = -1 } },
	{ MODIFIER1|SHIFT,      XK_j,         movetab,     { .i = -1 } },
	{ MODIFIER1|SHIFT,      XK_k,         movetab,     { .i = +1 } },
	{ MODIFIER1,            XK_Tab,       rotate,      { .i = 0 } },
	{ MODIFIER1,            XK_Shift_L,   showbar,     { .i = 1 } },
	{ SHIFT,                XK_Control_L, showbar,     { .i = 1 } },
	{ MODIFIER1,            XK_grave,     spawn,       SETPROP("_TABBED_SELECT_TAB") },
	{ MODIFIER1,            XK_1,         move,        { .i = 0 } },
	{ MODIFIER1,            XK_2,         move,        { .i = 1 } },
	{ MODIFIER1,            XK_3,         move,        { .i = 2 } },
	{ MODIFIER1,            XK_4,         move,        { .i = 3 } },
	{ MODIFIER1,            XK_5,         move,        { .i = 4 } },
	{ MODIFIER1,            XK_6,         move,        { .i = 5 } },
	{ MODIFIER1,            XK_7,         move,        { .i = 6 } },
	{ MODIFIER1,            XK_8,         move,        { .i = 7 } },
	{ MODIFIER1,            XK_9,         move,        { .i = 8 } },
	{ MODIFIER1,            XK_0,         move,        { .i = 9 } },
	{ MODIFIER1,            XK_q,         killclient,  { 0 } },
	{ MODIFIER1,            XK_u,         focusurgent, { 0 } },
	{ MODIFIER1|SHIFT,      XK_u,         toggle,      { .v = (void*) &urgentswitch } },
	{ 0,                    XK_F11,       fullscreen,  { 0 } },
};

static Key keyreleases[] = {
	/* modifier            key           function     argument */
    { MODIFIER1|SHIFT,     XK_Shift_L,   showbar,     { .i = 0 } },
	{ MODIFIER1|SHIFT,     XK_Control_L, showbar,     { .i = 0 } },
};
