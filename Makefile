# tabbed - tabbing interface
# See LICENSE file for copyright and license details.

include options.mk

SRC = tabbed.c xembed.c
OBJ = ${SRC:.c=.o}
BIN = ${OBJ:.o=}

all: options ${BIN}

options:
	@echo tabbed build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: options.h options.mk

.o:
	@echo CC -o $@
	@${CC} -o $@ $< ${LDFLAGS}
	@rm -f *.o

clean:
	@echo cleaning
	@rm -f ${BIN} ${OBJ} tabbed-spde-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p tabbed-spde-${VERSION}
	@cp -R LICENSE Makefile *.h *.mk \
		*.c tabbed-spde-${VERSION}
	@tar -cf tabbed-spde-${VERSION}.tar tabbed-spde-${VERSION}
	@gzip tabbed-spde-${VERSION}.tar
	@rm -rf tabbed-spde-${VERSION}

install: all
	@echo installing executable files to ${DESTDIR}${PREFIX}/bin
	@mkdir -p "${DESTDIR}${PREFIX}/bin"
	@cp -f ${BIN} "${DESTDIR}${PREFIX}/bin"
	@chmod 755 "${DESTDIR}${PREFIX}/bin/tabbed"

uninstall:
	@echo removing executable files from ${DESTDIR}${PREFIX}/bin
	@rm -f "${DESTDIR}${PREFIX}/bin/tabbed"
	@rm -f "${DESTDIR}${PREFIX}/bin/xembed"

.PHONY: all options clean dist install uninstall
